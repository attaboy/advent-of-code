// This solution is even simpler than the previous one.
// There is no need for a map, we can simply use array indecies
// as the timer keys.
// This also saves us from the hashing overhead of a map.
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func panicif(err error) {
	if err != nil {
		panic(err)
	}
}

func parseInput(input string) [9]int64 {
	fish := [9]int64{}
	input = strings.Trim(input, "\n")
	parts := strings.Split(input, ",")

	for _, v := range parts {
		i, err := strconv.Atoi(v)
		panicif(err)
		fish[i]++
	}

	return fish
}

const simDays = 256

func main() {
	bytes, err := os.ReadFile("input.txt")
	panicif(err)

	fish := parseInput(string(bytes))

	for i := 0; i < simDays; i++ {
		parentFish := fish[0]
		for j := 0; j < 8; j++ {
			fish[j] = fish[j+1]
		}
		fish[8] = parentFish  // baby fish
		fish[6] += parentFish // parent fish
	}

	var count int64
	for _, v := range fish {
		count += v
	}

	fmt.Printf("There are %d laternfish after %d days\n", count, simDays)
}

